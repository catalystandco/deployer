package main

import (
	"flag"
	"fmt"
	"github.com/AdRoll/goamz/aws"
	"github.com/AdRoll/goamz/s3"
	"os"
)

func createBucket(options optionsStorage, session *s3.S3) error {
	bucket := session.Bucket(options.Bucket)

	err := bucket.PutBucket("public-read")
	if err != nil {
		return err
	}

	err = bucket.PutBucketWebsite(s3.WebsiteConfiguration{
		IndexDocument: &s3.IndexDocument{
			Suffix: "index.html",
		},
		ErrorDocument: &s3.ErrorDocument{
			Key: "error.html",
		},
	})
	if err != nil {
		return err
	}

	return nil
}

func openS3(key, secret, region string) *s3.S3 {
	auth := aws.Auth{
		AccessKey: key,
		SecretKey: secret,
	}
	return s3.New(auth, aws.Regions[region])
}

type optionsStorage struct {
	Bucket string
	Root   string
	Dest   string
	Key    string
	Secret string
	Region string
}

func main() {

	options := optionsStorage{}
	set := flag.NewFlagSet(os.Args[1], flag.ExitOnError)
	set.StringVar(&options.Bucket, "bucket", "", "Where to deploy to")
	set.StringVar(&options.Root, "root", "./", "Which folder should I deploy")
	set.StringVar(&options.Dest, "dest", "./", "Where should I deploy to")
	set.StringVar(&options.Key, "key", "", "AWS IAM key")
	set.StringVar(&options.Secret, "secret", "", "AWS IAM secret")
	set.StringVar(&options.Region, "region", "ap-southeast-2", "Which region should I deploy to")
	set.Parse(os.Args[1:])

	s3Session := openS3(options.Key, options.Secret, options.Region)

	fmt.Printf("Creating Bucket %s\n", options.Bucket)
	err := createBucket(options, s3Session)

	if err != nil {
		fmt.Println("Error creating S3 bucket")
		fmt.Println(err)
		return
	}
}
